@extends('layouts.app')

@section('title')
	Bewerk {{ $user->name }}
@endsection

@section('content')
{!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('name', 'Naam', ['class' => 'control-label']) !!}
			{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'De naam hier']) !!}
		</div>
		<div class="col-sm-3">
			{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
			{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Het e-mailadres hier']) !!}
		</div>
		<div class="col-sm-3">
			{!! Form::label('role_id', 'Rol', ['class' => 'control-label']) !!}
			{!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
		</div>
		<div class="col-sm-3">
			{!! Form::label('location_id', 'Locatie', ['class' => 'control-label']) !!}
			{!! Form::select('location_id', $locations, null, ['class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
		</div>
            <div class="col-sm-3">
		{!! Form::label('postcode', 'Postcode', ['class' => 'control-label']) !!}
		{!! Form::text('postcode', null, ['class' => 'form-control', 'maxlength' => '6', 'placeholder' => 'bijvoorbeeld&#58; 2244AB']) !!}
	       </div>
            <div class="col-sm-1">
		{!! Form::label('number', 'Huisnummer', ['class' => 'control-label']) !!}
		{!! Form::text('number', null, ['class' => 'form-control', 'placeholder' => 'nr']) !!}
	       </div>
           <div class="col-sm-4">
		{!! Form::label('city', 'Stad', ['class' => 'control-label']) !!}
		{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'De stad hier']) !!}
	       </div>
           <div class="col-sm-3">
		{!! Form::label('street', 'Straat', ['class' => 'control-label']) !!}
		{!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'De Straat hier']) !!}
        </div>
    </div>
	<div class="form-group">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary">
				Opslaan
			</button>
		</div>
	</div>
{!! Form::close() !!}

@endsection
@section('scripts')
<script type="text/javascript">
$("#postcode").on('change', function() {
var postcode = $(this).val();
if (postcode.length == 6) {
console.log(postcode);
postcodeSearch(postcode);
}
});
function postcodeSearch() {
    var postcode = document.getElementById('postcode').value;
    var number = document.getElementById('number').value;
        console.log(postcode);
        console.log(number);
 $.ajax({
   url:"https://postcode-api.apiwise.nl/v2/addresses/?postcode=" + postcode + "&number=" + number,
   dataType: "json",
   type: "GET",
   headers: {
     "X-Api-Key": "il7gvOXGHS3arsC3QU8RcaLEZxQDlDhM5abSXIzb"
   },
   success: function(data) {
     console.log(data);
     for (i=0; i<data._embedded.addresses.length; i++) {
       document.getElementById('street').value = data._embedded.addresses[i].street;
       document.getElementById('city').value = data._embedded.addresses[i].city.label;
     }
   }
 });
}
</script>
@endsection'